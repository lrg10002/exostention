package lrg10002.mods.exostention.genetics;

import lrg10002.mods.exostention.util.ExoSubtypeBlock;
import lrg10002.mods.exostention.util.TemperatureHelper;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.world.World;

public class ExoGeneticsBlock extends ExoSubtypeBlock {


	public ExoGeneticsBlock(Material m) {
		super(m, new String[] { "temperatureControlUnit", "incubatorUnit", "fertilizationUnit", "microscopicDivisionUnit", "dictionaryUnit" });
	}
	
	@Override
	public boolean onBlockActivated(World w, int x, int y, int z, EntityPlayer p, int metadata, float what, float these, float are) {
		if (!w.isRemote) {
			p.addChatMessage(new ChatComponentText("You have clicked on " + this.getTrueLocalizedName(w, x, y, z)));
			TemperatureHelper.getCurrentTemperature(w, x, y, z);
			return true;
		}
		return false;
	}

	/*
	@Override
	public TileEntity createNewTileEntity(World w, int i) {
		if (i < 0 || i >= types.length) {
			return null;
		}

		switch (i) {
		case 0:
			return null; // TODO: return tempcontrolunit te
		case 1:
			return null;
		default:
			return null;
		}
	}
	*/


}
