package lrg10002.mods.exostention.genetics;

import lrg10002.mods.exostention.util.ExoEnergyTileEntity;
import net.minecraft.nbt.NBTTagCompound;

public class TileEntityTempControlUnit extends ExoEnergyTileEntity {
	
	public static final int MATRIX_WIDTH = 3;
	public static final int MATRIX_HEIGHT = 3;
	
	
	
	public TileEntityTempControlUnit() {
		super("temperatureControlUnitInventory", 80000, 9);
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt) {

		super.readFromNBT(nbt);
		

	}

	@Override
	public void writeToNBT(NBTTagCompound nbt) {

		super.writeToNBT(nbt);

		
	}

}
