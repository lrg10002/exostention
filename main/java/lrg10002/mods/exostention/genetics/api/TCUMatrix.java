package lrg10002.mods.exostention.genetics.api;

import java.util.ArrayList;
import java.util.List;

import lrg10002.mods.exostention.genetics.TileEntityTempControlUnit;
import net.minecraft.item.ItemStack;

public class TCUMatrix {
	
	private TileEntityTempControlUnit te;
	
	public TCUMatrix(TileEntityTempControlUnit t) {
		te = t;
	}
	
	public ItemStack getStack(int s) {
		return te.getStackInSlot(s);
	}
	
	public void setStack(int s, ItemStack is) {
		te.setInventorySlotContents(s, is);
	}
	
	public List<ItemStack> getAdjacentStacks(int s) {
		boolean hasLeft, hasRight, hasTop, hasBottom;
		ArrayList<ItemStack> islist = new ArrayList<ItemStack>();
		int column = s % te.MATRIX_WIDTH;
		int row = (int) Math.floor(s / te.MATRIX_HEIGHT);
		hasLeft = column > 0;
		hasRight = column < te.MATRIX_WIDTH - 1;
		hasTop = row > 0;
		hasBottom = row < te.MATRIX_HEIGHT - 1;
		
		if (hasLeft) {
			islist.add(getStack(s-1));
		}
		
		if (hasRight) {
			islist.add(getStack(s+1));
		}
		
		if (hasTop) {
			islist.add(getStack(s-te.MATRIX_WIDTH));
		}
		
		if (hasBottom) {
			
		}
		
	}

}
