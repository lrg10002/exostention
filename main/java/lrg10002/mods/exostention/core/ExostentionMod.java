package lrg10002.mods.exostention.core;

import lrg10002.mods.exostention.genetics.ExoGeneticsBlock;
import lrg10002.mods.exostention.util.ExoItemBlock;
import net.minecraft.block.material.Material;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;

@Mod(modid = ExostentionMod.MODID, version = ExostentionMod.VERSION)
public class ExostentionMod {
	public static final String MODID = "exostention";
	public static final String VERSION = "1.0";

	public static ExoGeneticsBlock geneticsBlock = (ExoGeneticsBlock) new ExoGeneticsBlock(Material.rock)
			.setBlockName("exogeneticsblock");

	@EventHandler
	public void init(FMLInitializationEvent event) {
		GameRegistry.registerBlock(geneticsBlock, ExoItemBlock.class, geneticsBlock.getUnlocalizedName()
				.replace("tile.", ""));
		GameRegistry.addShapelessRecipe(new ItemStack(geneticsBlock, 1, 1), new ItemStack(
				geneticsBlock, 1, 0));
		GameRegistry.addShapelessRecipe(new ItemStack(geneticsBlock, 1, 0), new ItemStack(
				Blocks.dirt, 1));
	}
}
