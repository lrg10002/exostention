package lrg10002.mods.exostention.util;

import java.util.List;

import lrg10002.mods.exostention.core.ExostentionMod;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.util.StatCollector;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ExoSubtypeBlock extends Block {

	public String[] types;
	public IIcon[] textures;

	public ExoSubtypeBlock(Material m, String[] t) {
		super(m);
		types = t;
	}

	@Override
	public int damageDropped(int m) {
		return m;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIcon(int side, int meta) {
		return textures[meta];
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister par1IconRegister) {
		textures = new IIcon[types.length];
		System.out.println("======================================================================="
				+ this.getUnlocalizedName());
		for (int i = 0; i < types.length; i++) {
			textures[i] = par1IconRegister.registerIcon(ExostentionMod.MODID + ":"
					+ this.getUnlocalizedName().replace("tile.", "") + "-" + types[i]);
		}
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void getSubBlocks(Item block, CreativeTabs creativeTabs, List list) {
		for (int i = 0; i < types.length; ++i) {
			list.add(new ItemStack(block, 1, i));
		}
	}

	public String getTrueLocalizedName(World w, int x, int y, int z) {
		return getTrueLocalizedName(w.getBlockMetadata(x, y, z));
	}

	public String getTrueLocalizedName(int m) {
		return StatCollector.translateToLocal(this.getUnlocalizedName()+"."+types[m]+".name");
	}

}
