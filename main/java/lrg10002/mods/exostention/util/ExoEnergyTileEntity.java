package lrg10002.mods.exostention.util;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraftforge.common.util.Constants;
import cofh.api.energy.EnergyStorage;
import cofh.api.energy.TileEnergyHandler;

public class ExoEnergyTileEntity extends TileEnergyHandler implements IInventory {

	public ItemStack[] inventory;
	public String name;

	public ExoEnergyTileEntity(String name, int storageSize, int inventorySize) {
		storage = new EnergyStorage(storageSize);
		inventory = new ItemStack[inventorySize];
		this.name = name;
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt) {

		super.readFromNBT(nbt);

		NBTTagList nbttaglist = nbt.getTagList("Items", Constants.NBT.TAG_COMPOUND);
		inventory = new ItemStack[getSizeInventory()];
		for (int i = 0; i < nbttaglist.tagCount(); i++) {
			NBTTagCompound nbttagcompound1 = nbttaglist.getCompoundTagAt(i);
			int j = nbttagcompound1.getByte("Slot") & 0xff;
			if (j >= 0 && j < inventory.length) {
				inventory[j] = ItemStack.loadItemStackFromNBT(nbttagcompound1);
			}
		}

		storage.readFromNBT(nbt);
	}

	@Override
	public void writeToNBT(NBTTagCompound nbt) {

		super.writeToNBT(nbt);

		NBTTagList nbttaglist = new NBTTagList();
		for (int i = 0; i < inventory.length; i++) {
			if (inventory[i] != null) {
				NBTTagCompound nbttagcompound1 = new NBTTagCompound();
				nbttagcompound1.setByte("Slot", (byte) i);
				inventory[i].writeToNBT(nbttagcompound1);
				nbttaglist.appendTag(nbttagcompound1);
			}
		}

		nbt.setTag("Items", nbttaglist);

		storage.writeToNBT(nbt);
	}

	@Override
	public int getSizeInventory() {
		return inventory.length;
	}

	@Override
	public ItemStack getStackInSlot(int s) {
		return s > 0 && s < inventory.length ? inventory[s] : null;
	}

	@Override
	public ItemStack decrStackSize(int s, int a) {
		if (s < 0 || s > inventory.length) {
			return null;
		}
		ItemStack is = inventory[s];
		int size = is.stackSize;
		if (size <= a) {
			ItemStack ret = inventory[s];
			inventory[s] = null;
			markDirty();
			return ret;
		} else {
			ItemStack ret = inventory[s].splitStack(a);
			if (inventory[s].stackSize <= 0) {
				inventory[s] = null;
			}
			markDirty();
			return ret;
		}
	}

	@Override
	public ItemStack getStackInSlotOnClosing(int s) {
		if (inventory[s] == null) {
			return null;
		}

		ItemStack is = inventory[s];
		inventory[s] = null;
		markDirty();
		return is;
	}

	@Override
	public void setInventorySlotContents(int s, ItemStack is) {
		inventory[s] = is;
		if (is != null && is.stackSize > getInventoryStackLimit()) {
			is.stackSize = getInventoryStackLimit();
		}
		markDirty();
	}

	@Override
	public String getInventoryName() {
		return name;
	}

	@Override
	public boolean hasCustomInventoryName() {
		return true;
	}

	@Override
	public int getInventoryStackLimit() {
		return 64;
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer entityplayer) {
		if (worldObj == null) {
			return true;
		}
		if (worldObj.getTileEntity(xCoord, yCoord, zCoord) != this) {
			return false;
		}
		return entityplayer.getDistanceSq((double) xCoord + 0.5D, (double) yCoord + 0.5D,
				(double) zCoord + 0.5D) <= 64D;
	}

	@Override
	public void openInventory() {

	}

	@Override
	public void closeInventory() {

	}

	@Override
	public boolean isItemValidForSlot(int var1, ItemStack var2) {
		return true;
	}
}
