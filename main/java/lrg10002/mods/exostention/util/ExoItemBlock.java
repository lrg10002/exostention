package lrg10002.mods.exostention.util;

import net.minecraft.block.Block;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;

public class ExoItemBlock extends ItemBlock {

	ExoSubtypeBlock theBlock;

	public ExoItemBlock(Block b) {
		super(b);
		theBlock = (ExoSubtypeBlock) b;
		setHasSubtypes(true);
	}

	@Override
	public String getUnlocalizedName(ItemStack itemStack) {
		int i = itemStack.getItemDamage();
		if (i < 0 || i >= theBlock.types.length) {
			i = 0;
		}

		return super.getUnlocalizedName() + "." + theBlock.types[i];
	}

	@Override
	public int getMetadata(int meta) {
		return meta;
	}

}
